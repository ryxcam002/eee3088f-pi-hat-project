# EEE3088F Pi Hat Project

A public repo for my group's EEE3088F Pi Hat design project. The theme of our design is an irrigation controller Pi Hat.

![](irrigation-hat-assembled-300x300.jpg)

## **Bill of Materials:**

### Amplifier Submodule

| Ref                          | Qnty | Value                  | Part                      | Datasheet | Description                                                                                            | Vendor |
| ---------------------------- | ---- | ---------------------- | ------------------------- | --------- | ------------------------------------------------------------------------------------------------------ | ------ |
| J1                           | 1    | Input Signal Voltage 1 | Connector:Conn_01x01_Male | ~         | Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/) |        |
| J2                           | 1    | Amplified Output 1     | Connector:Conn_01x01_Male | ~         | Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/) |        |
| J3                           | 1    | Input Signal Voltage 2 | Connector:Conn_01x01_Male | ~         | Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/) |        |
| J4                           | 1    | Amplified Output 2     | Connector:Conn_01x01_Male | ~         | Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/) |        |
| R1, R8                       | 2    | 3.6K                   | Device:R_Small_US         | ~         | Resistor, small US symbol                                                                              |        |
| R2, R4, R5, R6, R9, R11, R13 | 7    | 1K                     | Device:R_Small_US         | ~         | Resistor, small US symbol                                                                              |        |
| R3                           | 1    | 10K                    | Device:R_Small_US         | ~         | Resistor, small US symbol                                                                              |        |
| R7, R14                      | 2    | 2.5K                   | Device:R_Small_US         | ~         | Resistor, small US symbol                                                                              |        |
| R10                          | 1    | 8                      | Device:R_Small_US         | ~         | Resistor, small US symbol                                                                              |        |
| R12                          | 1    | 13                     | Device:R_Small_US         | ~         | Resistor, small US symbol                                                                              |        |
| U1, U2, U3, U4, U5, U6       | 6    | OPAMP                  | pspice:OPAMP              | ~         | OPAmp symbol for simulation only                                                                       |        |

### LED Submodule:

| Ref                                               | Qnty | Value                     | Part                       | Datasheet                                                                                                | Description                                                                                            | Vendor |
| ------------------------------------------------- | ---- | ------------------------- | -------------------------- | -------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------ | ------ |
| D1, D5                                            | 2    | Red LED                   | Device:LED                 | ~                                                                                                        | Light emitting diode                                                                                   |        |
| D2, D6                                            | 2    | Orange LED                | Device:LED                 | ~                                                                                                        | Light emitting diode                                                                                   |        |
| D3, D7                                            | 2    | Green LED                 | Device:LED                 | ~                                                                                                        | Light emitting diode                                                                                   |        |
| D4, D8                                            | 2    | DIODE                     | Simulation_SPICE:DIODE     | ~                                                                                                        | Diode, anode on pin 1, for simulation only!                                                            |        |
| J1                                                | 1    | Moisture Sensor 1 Voltage | Connector:Conn_01x01_Male  | ~                                                                                                        | Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/) |        |
| J2                                                | 1    | Raspberry_Pi_2_3          | Connector:Raspberry_Pi_2_3 | https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf | expansion header for Raspberry Pi 2 & 3                                                                |        |
| J3                                                | 1    | Moisture Sensor 2 Voltage | Connector:Conn_01x01_Male  | ~                                                                                                        | Generic connector, single row, 01x01, script generated (kicad-library-utils/schlib/autogen/connector/) |        |
| M1, M2                                            | 2    | Solenoid Valve 1          | Motor:Motor_DC             | ~                                                                                                        | DC Motor                                                                                               |        |
| Q1, Q2                                            | 2    | 2N2219                    | Transistor_BJT:2N2219      | http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF                                                   | 800mA Ic, 50V Vce, NPN Transistor, TO-39                                                               |        |
| R1, R2, R3, R5, R6, R7                            | 6    | 220                       | Device:R_Small_US          | ~                                                                                                        | Resistor, small US symbol                                                                              |        |
| R4, R8                                            | 2    | R_Small_US                | Device:R_Small_US          | ~                                                                                                        | Resistor, small US symbol                                                                              |        |
| RV1, RV2, RV3, RV4, RV5, RV6, RV7, RV8, RV9, RV10 | 10   | R_POT                     | Device:R_POT               | ~                                                                                                        | Potentiometer                                                                                          |        |
| U1, U2, U3, U4, U5, U11, U12, U13, U14, U15       | 10   | OPAMP                     | pspice:OPAMP               | ~                                                                                                        | OPAmp symbol for simulation only                                                                       |        |
| U6, U7, U16, U17                                  | 4    | NOT Gate                  | 74xGxx:74AHC1G04           | http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf                                                           | Single NOT Gate, Low-Voltage CMOS                                                                      |        |
| U8, U9, U18, U19                                  | 4    | AND Gate                  | 74xGxx:74LVC1G08           | http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf                                                           | Single AND Gate, Low-Voltage CMOS                                                                      |        |
| U10                                               | 1    | Switch                    | Analog_Switch:DG308AxY     | http://pdf.datasheetcatalog.com/datasheets/70/494502_DS.pdf                                              | Quad SPST Analog Switches, normally OFF, 60Ohm Ron, SOIC-16                                            |        |

### Power Submodule:

| Ref    | Qnty | Value               | Part                        | Datasheet                                                                                                  | Description                                                                                 | Vendor |
| ------ | ---- | ------------------- | --------------------------- | ---------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------- | ------ |
| C1     | 1    | 0.01µ               | Device:C_Small              | ~                                                                                                          | Unpolarized capacitor, small symbol                                                         |        |
| C2     | 1    | Cpar=100p, Rser=10k | Device:C_Small              | ~                                                                                                          | Unpolarized capacitor, small symbol                                                         |        |
| C3, C5 | 2    | 0.1µ                | Device:C_Small              | ~                                                                                                          | Unpolarized capacitor, small symbol                                                         |        |
| C4     | 1    | 4.7µ                | Device:C_Small              | ~                                                                                                          | Unpolarized capacitor, small symbol                                                         |        |
| C6     | 1    | 1n                  | Device:C_Small              | ~                                                                                                          | Unpolarized capacitor, small symbol                                                         |        |
| C7     | 1    | 150µ x3, Rser=0.1   | Device:C_Small              | ~                                                                                                          | Unpolarized capacitor, small symbol                                                         |        |
| L1     | 1    | 33µ, Rser=15m       | Device:L_Small              | ~                                                                                                          | Inductor, small symbol                                                                      |        |
| Q1     | 1    | Si4482DY            | Transistor_FET:BSC082N10LSG | http://www.infineon.com/dgdl/Infineon-BSC082N10LS-DS-v01_07-en.pdf?fileId=db3a3043163797a6011647faad240719 | 100A Id, 100V Vds, OptiMOS N-Channel Power MOSFET, 8.2mOhm Ron, Qg (typ) 78.0nC, PG-TDSON-8 |        |
| Q2, Q3 | 2    | BSC082N10LSG        | Transistor_FET:BSC082N10LSG | http://www.infineon.com/dgdl/Infineon-BSC082N10LS-DS-v01_07-en.pdf?fileId=db3a3043163797a6011647faad240719 | 100A Id, 100V Vds, OptiMOS N-Channel Power MOSFET, 8.2mOhm Ron, Qg (typ) 78.0nC, PG-TDSON-8 |        |
| R1     | 1    | 1Meg                | Device:R_Small_US           | ~                                                                                                          | Resistor, small US symbol                                                                   |        |
| R2     | 1    | 18.7k               | Device:R_Small_US           | ~                                                                                                          | Resistor, small US symbol                                                                   |        |
| R3, R7 | 2    | 511k                | Device:R_Small_US           | ~                                                                                                          | Resistor, small US symbol                                                                   |        |
| R4     | 1    | 30.1k               | Device:R_Small_US           | ~                                                                                                          | Resistor, small US symbol                                                                   |        |
| R5     | 1    | 75k                 | Device:R_Small_US           | ~                                                                                                          | Resistor, small US symbol                                                                   |        |
| R6     | 1    | 6m                  | Device:R_Small_US           | ~                                                                                                          | Resistor, small US symbol                                                                   |        |
| R8     | 1    | 36.5k               | Device:R_Small_US           | ~                                                                                                          | Resistor, small US symbol                                                                   |        |
| U1     | 1    | LTC3895             | LTC3895PartLib:LTC3895      |                                                                                                            |                                                                                             |        |

## **Usage Instructions:**

1.) Download the KiCAD design files to design your own PCB from the circuits that have been created.  
2.) Solder the components onto the board. (All components are SMD components)  
3.) Plug in your 12-24V DC power supply.  
4.) Attach the moisture sensors.  
5.) Attach the solenoid valves.  
6.) Plug in the Raspberry Pi via its microUSB power source.  
7.) Wait for the valves to activate once your soil is dry enough!

## **How to contribute:**

[-] Improve design of PCB and submit custom designs for review  
[-] Help us improve design efficiency  
[-] Leave comments and branch to add on other interseting submodules

## **Other Info**

[Test Link to section](#amplifier-submodule)
